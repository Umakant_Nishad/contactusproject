
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	if (session.getAttribute("userName") == null || session.getAttribute("password") == null) {

		response.sendRedirect("login.jsp");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<title>Insert title here</title>
<style type="text/css">
table {
	border-collapse: collapse;
	margin-left: auto;
	margin-right: auto;
	width: 60%;
	height: 10em;
	text-align: center;
}

table, td, th {
	border: 1px solid black;
}
</style>
</head>
<body>
	<div style="display: flex; justify-content: center;">
		<form action="requests.jsp" method="post">
			<label for="Archieved on archieved">Choose Option :</label> <select
				name="messageStatus">
				<option value="false">New Messages</option>
				<option value="true">Archived Messages</option>
			</select> <input type="submit" value="submit">
		</form>
	</div>



	<table style="margin-top: 20px;">
		<thead>
			<tr>
				<th>User ID</th>
				<th>User Name</th>
				<th>User Email</th>
				<th>Message</th>
				<th>Archived</th>
			</tr>
		</thead>
		<tbody>



			<%
				String str = request.getParameter("messageStatus");

				boolean value = Boolean.valueOf(str);

				Class.forName("org.postgresql.Driver");
				String url = "jdbc:postgresql://localhost:5432/postgres";
				String username = "postgres";
				String password = "root";

				String sqlQuery = null;
				try {
					Connection connection = DriverManager.getConnection(url, username, password);

					if (value) {
						sqlQuery = "SELECT * FROM contactus WHERE status='true'";
					} else {
						sqlQuery = "SELECT * FROM contactus WHERE status='false'";
					}
					Statement statement = connection.createStatement();
					ResultSet result = statement.executeQuery(sqlQuery);

					while (result.next()) {
			%>

			<tr>
				<td><%=result.getString("user_id")%></td>
				<td><%=result.getString("user_name")%></td>
				<td><%=result.getString("user_email")%></td>
				<td><%=result.getString("user_message")%></td>
				<td>
					<%
						if (!value) {
					%><button id="<%=result.getString("user_id")%>"
						onclick="archive(this.id)">Archived</button> <%
 	}
 %>
				</td>
			</tr>

			<%
				}

					connection.close();

				} catch (SQLException e) {

					throw new IllegalStateException("Cannot connect the database!", e);
				}
			%>
		</tbody>
	</table>

	<script type="text/javascript">
		function archive(id) {
			$.ajax({
				url : "/ContactUs/setArchieved",
				type : "post",
				data : {
					"id" : id
				},
				success : function(data) {

					location.reload();
				}

			});
		}
	</script>

</body>
</html>