package com.mountblue.web.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.management.Query;

import com.mountblue.web.model.Admin;
import com.mountblue.web.model.ContactUs;
import com.sun.corba.se.spi.orbutil.fsm.State;

public class Dao {
	private final String POSTGRES_DRIVER = "org.postgresql.Driver";
	private final String POSTGRES_URL = "jdbc:postgresql://localhost:5432/postgres";
	private final String POSTGRES_USER_NAME = "postgres";
	private final String POSTGRES_USER_PASSWORD = "root";

	private Connection getDatabaseConnection() throws ClassNotFoundException {

		Class.forName(POSTGRES_DRIVER);
		try {
			Connection connection = DriverManager.getConnection(POSTGRES_URL, POSTGRES_USER_NAME,
					POSTGRES_USER_PASSWORD);
			return connection;
		} catch (SQLException e) {

			throw new IllegalStateException("Cannot connect the database!", e);
		}

	}

	public void saveContactUsRequest(ContactUs contactUs) throws SQLException, ClassNotFoundException {

		String sqlQuery = "INSERT INTO contactus(user_name,user_email,user_message,status) VALUES('"
				+ contactUs.getName() + "', '" + contactUs.getEmail() + "','" + contactUs.getMessage() + "','false');";
		Connection connection = getDatabaseConnection();
		Statement statement = connection.createStatement();
		statement.executeUpdate(sqlQuery);
		connection.close();

	}

	public boolean verifyDetail(Admin loginDetail) throws SQLException, ClassNotFoundException {

		boolean status = false;

		String sqlQuery = "SELECT EXISTS (SELECT * FROM adminLogin WHERE user_name='" + loginDetail.getUserName()
				+ "'AND user_password='" + loginDetail.getPassword() + "')";

		Connection connection = getDatabaseConnection();
		Statement statement = connection.createStatement();
		ResultSet result = statement.executeQuery(sqlQuery);
		result.next();
		String value = result.getString("exists");

		if (value.equals("t")) {
			status = true;
		}
		connection.close();
		return status;
	}

	public void saveArchiveRequest(int id) throws ClassNotFoundException, SQLException {

		String sqlQuery = "UPDATE contactus SET status='true' WHERE user_id=" + id;
		Connection connection = getDatabaseConnection();
		Statement statement = connection.createStatement();
		statement.executeUpdate(sqlQuery);

		connection.close();

	}

}
