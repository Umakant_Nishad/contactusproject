package com.mountblue.web.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mountblue.web.dao.Dao;
import com.mountblue.web.model.Admin;

@WebServlet("/login")
public class AdminLoginController extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String userName = request.getParameter("userName");
		String password = request.getParameter("password");

		Admin loginDetail = new Admin();

		HttpSession session = request.getSession();

		session.setAttribute("userName", userName);
		session.setAttribute("password", password);

		loginDetail.setUserName(userName);
		loginDetail.setPassword(password);

		Dao dao = new Dao();

		try {
			boolean status = dao.verifyDetail(loginDetail);

			if (status) {

				RequestDispatcher dispatcher = request.getRequestDispatcher("requests.jsp");
				dispatcher.forward(request, response);

			} else {

				response.sendRedirect("login.jsp");
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
