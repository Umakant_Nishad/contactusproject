package com.mountblue.web.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mountblue.web.dao.Dao;
import com.mountblue.web.model.ContactUs;

@WebServlet("/contactUs")
public class ContactUsController extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String fullName = request.getParameter("fullName");
		String email = request.getParameter("email");
		String message = request.getParameter("message");

		ContactUs contactUsDetail = new ContactUs();

		contactUsDetail.setName(fullName);
		contactUsDetail.setEmail(email);
		contactUsDetail.setMessage(message);

		Dao dao = new Dao();

		try {
			dao.saveContactUsRequest(contactUsDetail);

			response.sendRedirect("/ContactUs/index.jsp");

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
